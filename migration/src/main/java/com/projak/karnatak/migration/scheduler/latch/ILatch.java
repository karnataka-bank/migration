package com.projak.karnatak.migration.scheduler.latch;

public interface ILatch {
    void countDown();
}